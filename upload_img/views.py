from turtle import title
from django.shortcuts import render
from django.http import JsonResponse

from upload_img.models import upload_img
from .forms import ImageFileUploadForm

from django.contrib.sites.shortcuts import get_current_site
# import .web_api.alarm_file
# import test.models.
from upload_img.forms import ImageFileUploadForm
from django.http import JsonResponse

import subprocess, os

import argparse
import glob
import os


# 檔案上傳
def image_upload_method(request):
    if request.method == "POST":
        form = ImageFileUploadForm(request.POST, request.FILES)
        # call .py（呼叫 AI Model）
        subprocess.Popen([
        'python',
        'static/test/inference_realesrgan.py',
        # '-i',
        # 'static/test/X4',
        ])
        if form.is_valid():
            form.save()
            print(form.cleaned_data)
            image = form.cleaned_data.get('選擇檔案')
            print(image)
            name = form.cleaned_data.get('檔案名稱')
            print(name)
            return JsonResponse({'error':False, 'message': '上傳成功'})
        else:
            return JsonResponse({'error':True, 'errors': form.errors})
    else:
        form = ImageFileUploadForm()
        return render(request, 'process.html', {'form': form})

def image_output(request):
    img_outpath = upload_img.object.get(title = title) # 用 title 尋找上傳時填寫的id
    return render(request, 'show.html',locals())


# # call .py（呼叫 AI Model）
# subprocess.Popen([
#     'python',
#     'static/test/inference_realesrgan.py',
#     # '-i',
#     # 'static/test/X4',
# ])

