from django.db import models

class upload_img(models.Model):
    img_title = models.CharField(max_length=200)
    img_path = models.ImageField(upload_to = "static/upload_img") #--> 關鍵
    img_process_path = models.CharField(max_length=200, null=True)

