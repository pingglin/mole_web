from django import forms
from upload_img.models import upload_img

class ImageFileUploadForm(forms.ModelForm):
    class Meta:
        model = upload_img
        fields = ('img_title', 'img_path',)
        
        # 改名
        labels = {
            'img_title': '檔案名稱',
            'img_path': '檔案位置',
        }
