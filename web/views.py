from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse
from .models import Message
from django.shortcuts import render, redirect
from django import forms

# 留言列表
class MessageList(ListView):
    model = Message
    ordering = ['-id']      # 以 id 欄位值由大至小反向排序

# 留言檢視
class MessageDetail(DetailView):
    model = Message
    fields = ['user', 'subject', 'content']  

# 新增留言
class MessageCreate(CreateView):
    model = Message
    fields = ['user', 'subject', 'content']     # 僅顯示 user, subject, content 這 3 個欄位
    success_url = '/message/'                   # 新增成功後，導向留言列表
    template_name = 'form.html'                 # 指定欲使用的頁面範本

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('user', 'subject', 'content')

def home(request):
    # 接收到資料的動作
    #   1. 存到資料庫
    #   2. 查詢"新"的留言內容
    if request.method == 'POST':
        form = MessageForm(request.POST)
        form.save()
        message_list = Message.objects.all().order_by('-publication_date')
        return render(request, 'book.html', locals())
    else:
    # "未"接收到資料的動作
    #   1. 建立表單
    #   2. 查詢資料庫中的資料
        form = MessageForm()
        message_list = Message.objects.all().order_by('-publication_date')
        return render(request, 'book.html', locals())


def base(request):
    return render(request, 'base.html', locals())

def process(request):
    return render(request, 'process.html', locals())

def about(request):
    return render(request, 'about.html', locals())

def contact(request):
    return render(request, 'contact.html', locals())



# def listone(request): 
#     try: 
#         unit = Message.objects.get(user = message.id) #讀取一筆資料
#     except:
#         errormessage = " (讀取錯誤!)"
#     return render(request, "contact.html", locals())

