from django.urls import path
from django.views.generic import RedirectView
from .views import *

urlpatterns = [
    path('', RedirectView.as_view(url='message/')),
    #path('message/', MessageList.as_view()),
    path('message/', home),  # 改成新的 view function
    path('message/<int:pk>/', MessageDetail.as_view()), 
    path('message/create/', MessageCreate.as_view()),  
]