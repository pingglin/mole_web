"""guestbook URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from cgitb import html
from django.contrib import admin
from django.urls import path, include
from web.views import *
from django.urls import path
from django.views.generic import RedirectView
from web.views import *
from upload_img.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', base),
    path('accounts/', include('django.contrib.auth.urls')),
    path('base/', base),
    path('about/', about),
    path('contact/', contact),
    # path('', RedirectView.as_view(url='message/')),
    #path('message/', MessageList.as_view()),
    path('message/', home),  # 改成新的 view function
    path('message/<int:pk>/', MessageDetail.as_view()), 
    path('message/create/', MessageCreate.as_view()),
    path('process/', image_upload_method), # 圖片上傳
    
    # 127.0.0.1/show/Test
    # => django 會透過 image_output 去找 title 是 Test 的照片
    path('show/<str:title>', image_output),  

]
